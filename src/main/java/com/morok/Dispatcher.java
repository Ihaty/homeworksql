package com.morok;

public class Dispatcher {

    private final TenantsRepository repository = new Tenants(new Properties());

    public void dispatch(int num) {
        switch (num){
            case 1:
                repository.numberOfInhabitants();
                break;
            case 2:
                repository.averageAge();
                break;
            case 3:
                repository.sortAlphabetically();
                break;
            case 4:
                repository.bInTheMiddle();
                break;
            case 5:
                repository.listOfHomeless();
                break;
            case 6:
                repository.avenuePravdaMinors();
                break;
            case 7:
                repository.streetAlphabeticalOutput();
                break;
            case 8:
                repository.sixLetterStreets();
                break;
            case 9:
                repository.sparselyPopulatedStreets();
        }
    }
}
