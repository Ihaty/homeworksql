package com.morok;

public interface TenantsRepository {
    void numberOfInhabitants();
    void averageAge();
    void sortAlphabetically();
    void bInTheMiddle();
    void listOfHomeless();
    void avenuePravdaMinors();
    void streetAlphabeticalOutput();
    void sixLetterStreets();
    void sparselyPopulatedStreets();
}
