package com.morok;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Tenants implements TenantsRepository {
    private final Properties properties;
    private static final Logger log = LoggerFactory.getLogger(Tenants.class);

    public Tenants(Properties properties) {
        this.properties = properties;
    }

    private String selectAllUsers() {
        return "SELECT * FROM people";
    }

    private String alphabeticallyLastName() {
        return "SELECT * FROM people ORDER BY last_name";
    }

    private String getHomeless() {
        return "select * from people where id_street is null";
    }

    private String getVictoryAndAge() {
        return "select * from people where id_street = 1 and age <= 14";
    }

    private String getStreetCount() {
        return "SELECT street.name_street, " +
                "COUNT(people.id_street) AS count FROM street JOIN people ON " +
                "street.id_street = people.id_street GROUP BY street.name_street " +
                "ORDER BY street.name_street";
    }

    private String getStreetSixLetter() {
        return "SELECT * FROM street WHERE LENGTH(name_street) = 6";
    }

    @Override
    public void numberOfInhabitants() {
        int quantity = 0;
        try (Statement statement = properties.connect().createStatement();
             ResultSet rs = statement.executeQuery(selectAllUsers())){
            while (rs.next()){
                quantity = rs.getRow();
            }
        } catch (SQLException e) {
            log.error("wrong {}", e.getMessage());
        }
        log.info("Inhabitants {}", quantity);
    }

    @Override
    public void averageAge() {
        int averAge = 0;
        int quantity = 0;
        try (Statement statement = properties.connect().createStatement();
             ResultSet rs = statement.executeQuery(selectAllUsers())){
            while (rs.next()){
                averAge += rs.getInt("age");
                quantity = rs.getRow();
            }
        } catch (SQLException e) {
            log.error("wrong {}", e.getMessage());
        }
        log.info("averageAge {}", averAge / quantity);
    }

    @Override
    public void sortAlphabetically() {
        try (Statement statement = properties.connect().createStatement();
             ResultSet rs = statement.executeQuery(alphabeticallyLastName())){
            while (rs.next()){
                log.info(rs.getString("last_name"));
            }
        } catch (SQLException e) {
            log.error("wrong {}", e.getMessage());
        }
    }

    @Override
    public void bInTheMiddle() {
        try (Statement statement = properties.connect().createStatement();
             ResultSet rs = statement.executeQuery(selectAllUsers())){
            while (rs.next()){
                String lName = rs.getString("last_name");
                char[] val = lName.replaceAll("\\s+","").toCharArray();
                int half = (val.length / 2);
                if(val[half] == 'b'){
                    log.info(lName);
                }
            }
        } catch (SQLException e) {
            log.error("wrong {}", e.getMessage());
        }
    }

    @Override
    public void listOfHomeless() {
        try (Statement statement = properties.connect().createStatement();
             ResultSet rs = statement.executeQuery(getHomeless())){
            while (rs.next()){
                log.info(rs.getString("first_name") + " " + rs.getString("last_name") + " " + rs.getInt("age"));
            }
        } catch (SQLException e) {
            log.error("wrong {}", e.getMessage());
        }
    }

    @Override
    public void avenuePravdaMinors() {
        try (Statement statement = properties.connect().createStatement();
             ResultSet rs = statement.executeQuery(getVictoryAndAge())){
            while (rs.next()){
                log.info(rs.getString("first_name") + " " + rs.getString("last_name") + " " + rs.getInt("age"));
            }
        } catch (SQLException e) {
            log.error("wrong {}", e.getMessage());
        }
    }

    @Override
    public void streetAlphabeticalOutput() {
        try (Statement statement = properties.connect().createStatement();
             ResultSet rs = statement.executeQuery(getStreetCount())){
            while (rs.next()){
                log.info(rs.getString("name_street") + " " + rs.getInt("count"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sixLetterStreets() {
        try (Statement statement = properties.connect().createStatement();
             ResultSet rs = statement.executeQuery(getStreetSixLetter())){
            while (rs.next()){
                log.info(rs.getString("name_street"));
            }
        } catch (SQLException e) {
            log.error("wrong {}", e.getMessage());
        }
    }

    @Override
    public void sparselyPopulatedStreets() {
        try (Statement statement = properties.connect().createStatement();
             ResultSet rs = statement.executeQuery(getStreetCount())){
            while (rs.next()){
                if(rs.getInt("count") <= 3) {
                    log.info(rs.getString("name_street"));
                }
            }
        } catch (SQLException e) {
            log.error("wrong {}", e.getMessage());
        }
    }
}
