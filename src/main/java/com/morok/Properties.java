package com.morok;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Properties {
    public static final String PostgreSQLURL = "jdbc:postgresql://localhost:5432/hw";
    public static final String PostgreSQLUSER = "postgres";
    public static final String PostgreSQLPASSWORD = "karlik18";

    public Connection connect() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(PostgreSQLURL, PostgreSQLUSER, PostgreSQLPASSWORD);
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
